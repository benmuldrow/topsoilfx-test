import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) {
        MainMenuBar menuBar = new MainMenuBar();
        MainButtonsBar buttonBar = new MainButtonsBar();
        Scene scene = new Scene(new VBox(), 750, 750);

        ((VBox) scene.getRoot()).getChildren().addAll(menuBar.getMenuBar(), buttonBar.getButtons());
        primaryStage.setTitle("Topsoil Test");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}